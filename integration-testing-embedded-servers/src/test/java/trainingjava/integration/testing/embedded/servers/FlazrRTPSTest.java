package trainingjava.integration.testing.embedded.servers;

import com.flazr.rtmp.client.RtmpClient;
import com.flazr.rtmp.server.RtmpServer;
import com.flazr.rtmp.server.ServerStop;
import java.io.*;
import java.util.Properties;
import lombok.extern.slf4j.Slf4j;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import org.junit.*;

@Slf4j
public class FlazrRTPSTest {

    @BeforeClass
    public static void startServer() throws Exception {
        createTemporaryDirectoryWithFlazrConfigAndVideoToStream();
        startServerInAnotherThread();
        waitALittleToLetServerStart();
    }

    @Test
    public void fetch_rtps_stream_from_rtps_server() {
        // This test does not assert anything for now...
        RtmpClient.main(new String[]{"barsandtone"});
    }

    @AfterClass
    public static void stopServer() throws Exception {
        ServerStop.main(null);
    }

    private static void createTemporaryDirectoryWithFlazrConfigAndVideoToStream() throws IOException {
        File rootTempDir = createDirectoryInTempDir("unittest");
        copyVideoFromTestResourcesForFlazrServing(rootTempDir);
        writeConfigurationFileForFlazr(rootTempDir);
        setFlazrBootDir(rootTempDir);
    }

    private static void startServerInAnotherThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    RtmpServer.main(null);
                } catch (Exception ex) {
                    log.error("Error starting server", ex);
                }
            }
        }).start();
    }

    private static void waitALittleToLetServerStart() throws InterruptedException {
        Thread.sleep(2000);
    }

    private static File createFolderAndCheckThatItExistsAfter(File rootTempDir, String subPath) {
        File f = new File(rootTempDir, subPath);
        boolean mkdirs = f.mkdirs();
        if (!mkdirs) {
            throw new IllegalStateException(
                    "Diretory " + subPath + " in " + rootTempDir + " was not succesfully created !");
        }
        return f;
    }

    private static void writeConfigurationFileForFlazr(File rootTempDir) throws IOException, FileNotFoundException {
        File confFolder = createFolderAndCheckThatItExistsAfter(rootTempDir, "/conf/");
        Properties p = new Properties();
        p.setProperty("server.home", rootTempDir.getAbsolutePath() + "/home");
        p.setProperty("server.port", "1935");
        p.setProperty("server.stop.port", "1934");
        p.setProperty("proxy.port", "8000");
        p.setProperty("proxy.stop.port", "7999");
        p.setProperty("proxy.remote.host", "127.0.0.1");
        p.setProperty("proxy.remote.port", "1935");
        FileOutputStream fileOutputStream = new FileOutputStream(
                new File(
                confFolder,
                "flazr.properties"));
        p.store(fileOutputStream, null);
        fileOutputStream.close();
    }

    private static void setFlazrBootDir(File rootTempDir) {
        System.setProperty("flazr.home", rootTempDir.getAbsolutePath());
    }

    private static void copyVideoFromTestResourcesForFlazrServing(File tempFolderRoot) throws IOException {
        InputStream videoStreamFromTestResources = FlazrRTPSTest.class.getResourceAsStream("/barsandtone.flv");
        File vidsFolder = createFolderAndCheckThatItExistsAfter(tempFolderRoot, "/home/apps/vod/");

        copyInputStreamToFile(videoStreamFromTestResources, new File(vidsFolder, "barsandtone.flv"));
    }

    private static File createDirectoryInTempDir(String dirFullSubPath) throws IOException {
        File baseFile = File.createTempFile(dirFullSubPath, "");
        baseFile.delete();
        boolean mkdirs = baseFile.mkdirs();
        if (!mkdirs) {
            throw new IllegalStateException("Diretory " + baseFile + " was not succesfully created !");
        }
        return baseFile;
    }
}
